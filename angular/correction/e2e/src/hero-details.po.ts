import { browser, by, element } from 'protractor';

export class HeroDetailPage {
  navigateTo() {
    return browser.get(`${browser.baseUrl}/heroes/detail/12`) as Promise<any>;
  }

  getTitleText() {
    return element(by.css('h2')).getText() as Promise<string>;
  }
}
