import { browser, logging } from 'protractor';
import { HeroListPage } from './hero-list.po';

describe('HeroListPage', () => {
  let heroListPage;

  beforeEach(() => {
    heroListPage = new HeroListPage();
  });

  it('should navigate', () => {
    heroListPage.navigateTo();
    expect(heroListPage.getTitleText()).toEqual('My Heroes');
  });


  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
