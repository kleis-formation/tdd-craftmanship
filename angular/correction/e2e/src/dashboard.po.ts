import { browser, by, element } from 'protractor';

export class DashboardPage {
  navigateTo() {
    return browser.get(`${browser.baseUrl}/heroes`) as Promise<any>;
  }

  getTitleText() {
    return element(by.css('h2')).getText() as Promise<string>;
  }
}
