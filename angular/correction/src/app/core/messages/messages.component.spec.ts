import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MessagesComponent } from './messages.component';
import { MockComponent, MockModule } from 'ng-mocks';
import { MatListModule } from '@angular/material/list';
import { AccentButtonComponent } from '../../shared/accent-button/accent-button.component';
import { MatDividerModule } from '@angular/material/divider';
import { By } from '@angular/platform-browser';
import { MessagesService } from './shared/messages.service';
import { DebugElement } from '@angular/core';

describe('MessagesComponent', () => {
  let component: MessagesComponent;
  let fixture: ComponentFixture<MessagesComponent>;
  let messageService: MessagesService;
  let messageSpy: jasmine.SpyObj<any>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        MessagesComponent,
        MockComponent(AccentButtonComponent),
      ],
      imports: [
        MockModule(MatListModule),
        MockModule(MatDividerModule),
      ],
      providers: [
        {
          provide: MessagesService,
          useValue: {
            clear: () => {},
            getMessages: () => [ 'a', 'b' ],
          },
        },
      ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    messageService = TestBed.inject(MessagesService);
    messageSpy = spyOn(messageService, 'clear');
    fixture = TestBed.createComponent(MessagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have an h2 title', () => {
    const h2 = fixture.debugElement.query(By.css('h2'));
    expect(h2.nativeElement.textContent).toBe('Messages');
  });

  it('should have an accent button to clear messages', () => {
    const button = fixture.debugElement.query(By.css('app-accent-button'));
    expect(button.nativeElement.attributes.label.textContent).toBe('clear');
  });

  it('should display all messages', () => {
    const listItems: DebugElement[] = fixture.debugElement.queryAll(By.css('mat-list-item'));
    expect(listItems.length).toBe(2);
  });

  it('clicking on clear button should call messageService.clear()', () => {
    const button = fixture.debugElement.query(By.css('app-accent-button'));

    // When
    button.nativeElement.click();
    fixture.detectChanges();

    // Then
    expect(messageSpy).toHaveBeenCalled();
  });
});
