import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderModule } from './header/header.module';
import { MessagesModule } from './messages/messages.module';
import { MessagesComponent } from './messages/messages.component';
import { HeaderComponent } from './header/header.component';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    HeaderModule,
    MessagesModule,
  ],
  exports: [ MessagesComponent, HeaderComponent ],
})
export class CoreModule {}
