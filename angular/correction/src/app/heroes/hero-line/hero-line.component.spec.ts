import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeroLineComponent } from './hero-line.component';
import { RouterTestingModule } from '@angular/router/testing';
import { MockComponents } from 'ng-mocks';
import { DeleteIconButtonComponent } from '../../shared/delete-icon-button/delete-icon-button.component';
import { RouterLinkComponent } from '../../shared/link/router-link.component';
import { By } from '@angular/platform-browser';

describe('HeroLineComponent', () => {
  let component: HeroLineComponent;
  let fixture: ComponentFixture<HeroLineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        HeroLineComponent,
        MockComponents(
          DeleteIconButtonComponent,
          RouterLinkComponent,
        ),
      ],
      imports: [ RouterTestingModule ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeroLineComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    component.hero = { id: 0, name: 'Toto' };
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });

  it('should emit delete event when clicking on delete button', () => {
    // Given
    const deleteSpy = spyOn(component.delete, 'emit');
    component.hero = { id: 0, name: 'Toto' };
    fixture.detectChanges();

    // When
    const deleteButton = fixture.debugElement.query(By.css('app-delete-icon-button'));
    deleteButton.nativeElement.click();

    // Then
    expect(deleteSpy).toHaveBeenCalledWith({ id: 0, name: 'Toto' });
  });

  it('should emit select event when clicking on a link', () => {
    // Given
    const selectSpy = spyOn(component.select, 'emit');
    component.hero = { id: 0, name: 'Toto' };
    fixture.detectChanges();

    // When
    const link = fixture.debugElement.query(By.css('app-router-link'));
    link.nativeElement.click();

    // Then
    expect(selectSpy).toHaveBeenCalledWith({ id: 0, name: 'Toto' });
  });
});
