import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeroDetailComponent } from './hero-detail.component';
import { ActivatedRoute } from '@angular/router';
import { HeroService } from '../shared/hero.service';
import { of } from 'rxjs';
import { Hero } from '../shared/hero.model';
import { By } from '@angular/platform-browser';
import { MockComponent, MockModule } from 'ng-mocks';
import { MatFormFieldModule } from '@angular/material/form-field';
import { AccentButtonComponent } from '../../shared/accent-button/accent-button.component';
import { PrimaryButtonComponent } from '../../shared/primary-button/primary-button.component';
import { FormsModule } from '@angular/forms';
import { Location } from '@angular/common';


describe('HeroDetailComponent', () => {
  let component: HeroDetailComponent;
  let fixture: ComponentFixture<HeroDetailComponent>;
  let heroService: HeroService;
  let location: Location;
  let activatedRoute: ActivatedRoute;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        HeroDetailComponent,
        MockComponent(AccentButtonComponent),
        MockComponent(PrimaryButtonComponent),
      ],
      imports: [
        MockModule(MatFormFieldModule),
        FormsModule,
      ],
      providers: [
        {
          provide: Location,
          useValue: {
            back: () => {},
          },
        },
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              paramMap: {
                get: id => 1,
              },
            },
          },
        },
        {
          provide: HeroService,
          useValue: {
            getHero: id => of({ id: 1, name: 'Toto' } as Hero),
            updateHero: () => of(true),
          },
        },
      ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    heroService = TestBed.inject(HeroService);
    location = TestBed.inject(Location);
    activatedRoute = TestBed.inject(ActivatedRoute);
    fixture = TestBed.createComponent(HeroDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('when no hero, main div should not be displayed', () => {
    // Given
    component.hero = undefined;
    fixture.detectChanges();

    // When
    const heroDiv = fixture.debugElement.query(By.css('div'));

    // Then
    expect(heroDiv).toBeNull();
  });

  it('when a hero, main div should be displayed', () => {
    // Given
    component.hero = { id: 0, name: 'Toto' };
    fixture.detectChanges();

    // When
    const heroDiv = fixture.debugElement.query(By.css('div'));

    // Then
    expect(heroDiv).toBeTruthy();
  });

  it('the div title should contains the hero name in uppercase', () => {
    // Given
    component.hero = { id: 0, name: 'Toto' };
    fixture.detectChanges();

    // When
    const heroDiv = fixture.debugElement.query(By.css('h2'));

    // Then
    expect(heroDiv.nativeElement.innerText).toContain('TOTO');
  });

  it('writing in input should update hero name', () => {
    // Given
    component.hero = { id: 0, name: '' };
    fixture.detectChanges();

    // When
    const input = fixture.debugElement.query(By.css('input'));
    input.nativeElement.value = 'toto';
    input.nativeElement.dispatchEvent(new Event('input'));

    fixture.detectChanges();
    expect(component.hero.name).toBe('toto');
  });

  it('clicking on back button should call location.back', () => {
    // Given
    const locationSpy = spyOn(location, 'back');

    // When
    const backButton = fixture.debugElement.query(By.css('app-primary-button'));
    backButton.nativeElement.click();

    fixture.detectChanges();
    expect(locationSpy).toHaveBeenCalled();
  });

  it('click on save button should call save function', () => {
    // Given
    const saveSpy = spyOn(component, 'save');

    // When
    const saveButton = fixture.debugElement.query(By.css('app-accent-button'));
    saveButton.nativeElement.click();

    fixture.detectChanges();
    expect(saveSpy).toHaveBeenCalled();
  });

  it('save method should call hero service update method', () => {
    // Given
    const heroSpy = spyOn(heroService, 'updateHero').and.returnValue(of());

    component.hero = { id: 0, name: 'toto' };
    fixture.detectChanges();

    // When
    component.save();

    // Then
    expect(heroSpy).toHaveBeenCalledWith({ id: 0, name: 'toto' });
  });

  it('getHero should extract id from route', () => {
    // Given
    const routeSpy = spyOn(activatedRoute.snapshot.paramMap, 'get').and.returnValue('1');

    // When
    component.getHero();

    // Then
    expect(routeSpy).toHaveBeenCalledWith('id');
  });

  it('getHero should call hero service', () => {
    // Given
    spyOn(activatedRoute.snapshot.paramMap, 'get').and.returnValue('1');
    const heroServiceSpy = spyOn(heroService, 'getHero').and.returnValue(of({ id: 0, name: 'Toto' }));

    // When
    component.getHero();

    // Then
    expect(heroServiceSpy).toHaveBeenCalledWith(1);
  });
});
