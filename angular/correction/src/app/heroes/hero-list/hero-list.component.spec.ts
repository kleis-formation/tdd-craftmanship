import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeroListComponent } from './hero-list.component';
import { RouterTestingModule } from '@angular/router/testing';
import { HeroService } from '../shared/hero.service';
import { of } from 'rxjs';
import { MockComponent, MockModule } from 'ng-mocks';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FormsModule } from '@angular/forms';
import { AccentButtonComponent } from '../../shared/accent-button/accent-button.component';
import { HeroLineComponent } from '../hero-line/hero-line.component';
import { MatListModule } from '@angular/material/list';
import { By } from '@angular/platform-browser';
import { MatDividerModule } from '@angular/material/divider';
import { HeroDetailComponent } from '../hero-detail/hero-detail.component';
import { Router } from '@angular/router';

describe('HeroListComponent', () => {
  let component: HeroListComponent;
  let fixture: ComponentFixture<HeroListComponent>;
  let heroService: HeroService;
  let addHeroSpy: jasmine.SpyObj<any>;
  let deleteHeroSpy: jasmine.SpyObj<any>;
  let router: Router;
  let routerSpy: jasmine.SpyObj<any>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        HeroListComponent,
        MockComponent(AccentButtonComponent),
        MockComponent(HeroLineComponent),
      ],
      imports: [
        RouterTestingModule.withRoutes([
          {
            path: 'heroes/detail/1',
            component: MockComponent(HeroDetailComponent),
          },
        ]),
        MockModule(MatFormFieldModule),
        MockModule(MatListModule),
        MockModule(MatDividerModule),
        FormsModule,
      ],
      providers: [
        {
          provide: HeroService,
          useValue: {
            getHeroes: () => of([]),
            addHero: hero => {},
            deleteHero: hero => {},
          },
        },
      ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeroListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    heroService = TestBed.inject(HeroService);
    addHeroSpy = spyOn(heroService, 'addHero').and.returnValue(of({ id: 0, name: 'toto' }));
    deleteHeroSpy = spyOn(heroService, 'deleteHero').and.returnValue(of());
    router = TestBed.inject(Router);
    routerSpy = spyOn(router, 'navigateByUrl');
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have an h2 title with My Heroes', () => {
    const h2 = fixture.debugElement.query(By.css('h2'));
    expect(h2.nativeElement.textContent).toBe('My Heroes');
  });

  it('should an input for the hero name', () => {
    const input = fixture.debugElement.query(By.css('input'));
    expect(input).toBeTruthy();
  });

  describe('Add hero', () => {

    it('should have a button to add a new hero', () => {
      const addHeroButton = fixture.debugElement.query(By.css('app-accent-button'));
      expect(addHeroButton).toBeTruthy();
    });

    it('clicking on the add hero button not call heroService.addHero if input value is empty', () => {
      const addHeroButton = fixture.debugElement.query(By.css('app-accent-button'));

      // When
      addHeroButton.nativeElement.click();
      fixture.detectChanges();

      // Then
      expect(addHeroSpy).not.toHaveBeenCalled();
    });

    it('clicking on the add hero button should call heroService.addHero', () => {
      const input = fixture.debugElement.query(By.css('input'));
      const addHeroButton = fixture.debugElement.query(By.css('app-accent-button'));

      // When
      input.nativeElement.value = 'toto';
      addHeroButton.nativeElement.click();
      fixture.detectChanges();

      // Then
      expect(addHeroSpy).toHaveBeenCalledWith({ name: 'toto' });
    });

    it('clicking on the add hero button should add a hero to the list', () => {
      // Given
      const input = fixture.debugElement.query(By.css('input'));
      const addHeroButton = fixture.debugElement.query(By.css('app-accent-button'));
      component.heroes = [];
      fixture.detectChanges();

      // When
      input.nativeElement.value = 'toto';
      addHeroButton.nativeElement.click();
      fixture.detectChanges();

      // Then
      expect(component.heroes.length).toBe(1);
    });
  });

  describe('Hero line', () => {

    it('should display one line by hero', () => {
      component.heroes = [ { id: 0, name: 'toto' }, { id: 1, name: 'titi' } ];
      fixture.detectChanges();
      const lines = fixture.debugElement.queryAll(By.css('app-hero-line'));
      expect(lines.length).toBe(2);
    });

    it('should have a delete action', () => {
      // Given
      component.heroes = [ { id: 0, name: 'toto' } ];
      fixture.detectChanges();
      const heroLine = fixture.debugElement.query(By.css('app-hero-line'));
      expect(heroLine.listeners.find(listener => listener.name === 'delete')).toBeTruthy();
    });

    it('should have a select action', () => {
      // Given
      component.heroes = [ { id: 0, name: 'toto' } ];
      fixture.detectChanges();
      const heroLine = fixture.debugElement.query(By.css('app-hero-line'));
      expect(heroLine.listeners.find(listener => listener.name === 'select')).toBeTruthy();
    });
  });


  describe('Delete', () => {

    it('delete method should call heroService.deleteHero', () => {
      // Given
      component.heroes = [ { id: 0, name: 'toto' }, { id: 1, name: 'titi' } ];
      fixture.detectChanges();

      // When
      component.delete({ id: 0, name: 'toto' });

      // Then
      expect(deleteHeroSpy).toHaveBeenCalledWith({ id: 0, name: 'toto' });
    });

    it('navigate should call router with hero detailpath', () => {
      // Given
      component.navigate(1);

      expect(routerSpy).toHaveBeenCalledWith('heroes/detail/1');
    });
  });

});
