import { async, TestBed } from '@angular/core/testing';
import { HeroService } from './hero.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { MessagesService } from '../../core/messages/shared/messages.service';
import { environment } from '../../../environments/environment';
import { Hero } from './hero.model';

describe('HeroService', () => {
  const url: string = environment.heroesUrl;
  const searchUrl = environment.heroesSearchByNameUrl;
  let sut: HeroService;
  let http: HttpTestingController;
  let messageService;
  let logSpy;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule ],
      providers: [
        HeroService,
        {
          provide: MessagesService,
          useValue: {
            add: message => {},
            clear: () => {},
          },
        },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    sut = TestBed.inject(HeroService);
    http = TestBed.inject(HttpTestingController);
    messageService = TestBed.inject(MessagesService);
    logSpy = spyOn(messageService, 'add');
  });

  it('should be created', () => {
    expect(sut).toBeTruthy();
  });

  describe('getHeroes', () => {
    it('should call /heroes and return a list of heroes', () => {
      // Given
      const expected: Hero[] = [ { id: 0, name: 'Toto' } ];

      // When
      sut.getHeroes().subscribe(response => {
        expect(response).toBe(expected);
      });

      // Then
      http.expectOne({
        url,
        method: 'GET',
      }).flush(expected);
    });

  });

  describe('getTopHeroes', () => {
    it('should call GET /heroes and return top 4 first heroes', () => {
      // Given
      const heroes: Hero[] = [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 ]
        .map(index => ({ id: index, name: `Toto-${index}` }))
        .reduce((prec, curr) => ([ ...prec, curr ]), []);

      // When
      sut.getTopHeroes().subscribe(response => {
        expect(response.length).toBe(4);
      });

      // Then
      http.expectOne({
        url,
        method: 'GET',
      }).flush(heroes);
    });
  });

  describe('getHero', () => {
    it('should call GET heroes/{id} and return a single hero', () => {
      // Given
      const hero: Hero = { id: 0, name: 'Toto' };

      // When
      sut.getHero(1).subscribe(response => {
        expect(response).toBe(hero);
      });

      // Then
      http.expectOne({
        url: `${url}1`,
        method: 'GET',
      }).flush(hero);
    });
  });

  describe('updateHero', () => {
    it('should call PUT heroes/{id} and call message service', () => {
      // Given
      const hero: Hero = { id: 1, name: 'Toto' };

      // When
      sut.updateHero(hero).subscribe(response => {
        expect(logSpy).toHaveBeenCalledWith(`HeroService: updated hero id=1`);
      });

      // Then
      http.expectOne({
        url: `${url}1`,
        method: 'PUT',
      }).flush(hero);
    });
  });

  describe('addHero', () => {
    it('should call POST heroes and return the new hero', () => {
      // Given
      const hero: Hero = { id: 1, name: 'Toto' };

      // When
      sut.addHero(hero).subscribe(response => {
        expect(response).toBe(hero);
      });

      // Then
      http.expectOne({
        url,
        method: 'POST',
      }).flush(hero);
    });
  });

  describe('deleteHero', () => {
    it('should call DELETE heroes/{id} and call message service', () => {
      // Given
      const hero: Hero = { id: 1, name: 'Toto' };

      // When
      sut.deleteHero(hero).subscribe(response => {
        expect(logSpy).toHaveBeenCalledWith('HeroService: deleted hero id=1');
      });

      // Then
      http.expectOne({
        url: `${url}1`,
        method: 'DELETE',
      }).flush(hero);
    });
  });

  describe('searchHeroes', () => {
    it('should call GET heroes and return heroes matching search term', () => {
      // Given
      const heroes: Hero[] = [
        { id: 1, name: 'Toto' },
        { id: 2, name: 'Tata' },
        { id: 3, name: 'Titi' },
      ];

      // When
      sut.searchHeroes('to').subscribe(response => {
        expect(response[0]).toEqual({ id: 1, name: 'Toto' });
      });

      // Then
      http.expectOne({
        url: `${searchUrl}to`,
        method: 'GET',
      }).flush(heroes);
    });

    it('should NOT call GET heroes when empty search term', () => {
      // When
      sut.searchHeroes('    ')
        .subscribe(response => {
          expect(response.length).toBe(0);
        });

      // Then
      http.expectNone({
        url: `${searchUrl}to`,
        method: 'GET',
      });
    });

    it('shoudl call log service when empty response', () => {
      // When
      sut.searchHeroes('a')
        .subscribe(response => {
          expect(logSpy).toHaveBeenCalledWith('HeroService: no heroes matching "a"');
        });

      // Then
      http.expectOne({
        url: `${searchUrl}a`,
        method: 'GET',
      }).flush([]);
    });
  });

  describe('Http errors', () => {

    it('searchHeroes : when an error occurs, should call the log service', () => {
      // Given
      const expectedErrorMessage = 'HeroService: searchHeroes failed: Http failure response for http://localhost:4300/api/heroes?name_like=to: 400 something whent wrong';

      // When
      sut.searchHeroes('to')
        .subscribe(() => {
          expect(logSpy).toHaveBeenCalledWith(expectedErrorMessage);
        });

      // Then
      http.expectOne({
        url: `${searchUrl}to`,
        method: 'GET',
      }).flush({}, { status: 400, statusText: 'something whent wrong' });
    });

    it('getHeroes : when an error occurs, should call the log service', () => {
      // When
      sut.getHeroes()
        .subscribe(() => {
          expect(logSpy).toHaveBeenCalled();
        });

      // Then
      http.expectOne({
        url: `${url}`,
        method: 'GET',
      }).flush({}, {
        status: 400,
        statusText: 'something whent wrong',
      });
    });

    it('getHero : when an error occurs, should call the log service', () => {
      // When
      sut.getHero(1)
        .subscribe(() => {
          expect(logSpy).toHaveBeenCalled();
        });

      // Then
      http.expectOne({
        url: `${url}1`,
        method: 'GET',
      }).flush({}, {
        status: 400,
        statusText: 'something whent wrong',
      });
    });

    it('addHero : when an error occurs, should call the log service', () => {
      // When
      sut.addHero({ id: 0, name: 'toto' })
        .subscribe(() => {
          expect(logSpy).toHaveBeenCalled();
        });

      // Then
      http.expectOne({
        url: `${url}`,
        method: 'POST',
      }).flush({}, {
        status: 400,
        statusText: 'something whent wrong',
      });
    });

    it('deleteHero : when an error occurs, should call the log service', () => {
      // When
      sut.deleteHero(1)
        .subscribe(() => {
          expect(logSpy).toHaveBeenCalled();
        });

      // Then
      http.expectOne({
        url: `${url}1`,
        method: 'DELETE',
      }).flush({}, {
        status: 400,
        statusText: 'something whent wrong',
      });
    });

    it('updateHero : when an error occurs, should call the log service', () => {
      // When
      sut.updateHero({ id: 1, name: 'toto' })
        .subscribe(() => {
          expect(logSpy).toHaveBeenCalled();
        });

      // Then
      http.expectOne({
        url: `${url}1`,
        method: 'PUT',
      }).flush({}, {
        status: 400,
        statusText: 'something whent wrong',
      });
    });

  });


});
