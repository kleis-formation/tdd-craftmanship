import { Injectable } from '@angular/core';
import { HeroService } from './hero.service';
import { Hero } from './hero.model';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable()
export class TopHeroesResolver implements Resolve<Hero[]> {

  constructor(private heroService: HeroService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Hero[]> {
    return this.heroService.getTopHeroes();
  }
}
