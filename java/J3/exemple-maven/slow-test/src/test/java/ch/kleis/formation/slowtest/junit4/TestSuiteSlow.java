package ch.kleis.formation.slowtest.junit4;

import org.junit.extensions.cpsuite.ClasspathSuite;
import org.junit.runner.RunWith;

@RunWith(ClasspathSuite.class)
@ClasspathSuite.ClassnameFilters({"ch.kleis.formation.*.*SlowTest"})
public class TestSuiteSlow {

}


