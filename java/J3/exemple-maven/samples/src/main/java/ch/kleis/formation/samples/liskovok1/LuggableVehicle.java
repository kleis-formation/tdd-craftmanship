package ch.kleis.formation.samples.liskovok1;


import java.util.ArrayList;
import java.util.List;

public abstract class LuggableVehicle extends Vehicle {
    // Impossible dans ce cas de voir uniquement le coté 'Luggagable'

    List<Luggage> luggages = new ArrayList<>();

    public void storeLuggable(Luggage newLuggage) {
        luggages.add(newLuggage);
    }
}


