package ch.kleis.formation.samples.liskovok3;


import java.util.List;

public interface LuggableVehicle {

    public default void storeLuggable(Luggage luggage){
        getLuggages().add(luggage);
        // Or more complexe
    }

    public List<Luggage> getLuggages();
}

