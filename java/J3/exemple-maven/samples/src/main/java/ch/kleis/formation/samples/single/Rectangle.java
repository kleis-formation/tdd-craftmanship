package ch.kleis.formation.samples.single;

public class Rectangle {
    protected int height;
    protected int width;

    public void setWidth(int width) {
        this.width = width;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int computeArea(){
        return height * width;
    }

}

