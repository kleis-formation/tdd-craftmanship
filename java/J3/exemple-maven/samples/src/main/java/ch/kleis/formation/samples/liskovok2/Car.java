package ch.kleis.formation.samples.liskovok2;

import java.util.ArrayList;
import java.util.List;

public class Car extends Vehicle implements LuggableVehicle {
    List<Luggage> luggages = new ArrayList<>();


    public  void storeLuggable(Luggage luggage){
        luggages.add(luggage);
        // Or more complexe
    }

}

