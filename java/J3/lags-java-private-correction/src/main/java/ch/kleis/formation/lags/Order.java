package ch.kleis.formation.lags;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Order implements Comparable<Order> {

    public static final DateTimeFormatter ORDER_DATE_FORMAT = DateTimeFormatter.ofPattern("yyyyDDD");

    private String id;
    private LocalDate begin;
    private LocalDate end;
    private double price;


    public Order(String id, LocalDate begin, int elapseInDays, double price) {
        this.id = id;
        this.begin = begin;
        this.end = begin.plusDays(elapseInDays-1);
        this.price = price;
    }

    public Order(String id, String date, int elapseInDays, double price) {
        this(id, LocalDate.parse(date, ORDER_DATE_FORMAT), elapseInDays, price);
    }

    public String getId() {
        return this.id;
    }

    private int getElapseInDays() {
        return end.getDayOfYear()-begin.getDayOfYear()+1;
    }

    public double getPrice() {
        return this.price;
    }

    public boolean hasOverlayWith(Order other) {
        return (end.compareTo(other.begin)>0 && other.end.compareTo(begin)>0);
    }

    public int compareTo(Order other) {
        return this.begin.compareTo(other.begin);
    }

    public boolean appendsBefore(Order other){
        return end.compareTo(other.begin) < 0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Order order = (Order) o;

        if (end == null || !end.equals(order.end)) return false;
        if (Double.compare(order.price, price) != 0) return false;
        if (id != null ? !id.equals(order.id) : order.id != null) return false;
        return begin != null ? begin.equals(order.begin) : order.begin == null;

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = id != null ? id.hashCode() : 0;
        result = 31 * result + (begin != null ? begin.hashCode() : 0);
        result = 31 * result + (end != null ? end.hashCode() : 0);
        temp = Double.doubleToLongBits(price);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    public String toString(String format) {
        return String.format(format, id, begin.format(ORDER_DATE_FORMAT), this.getElapseInDays(), price);
    }
}
