package ch.kleis.formation.lags;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;

public class OrderFileDao implements OrderDao {

    public static final String ORDERS_FILENAME = "ORDRES.CSV";

    private final OrderCsvParser csvParser;
    private String fileName;

    public OrderFileDao(OrderCsvParser parser) {
        this.fileName = ORDERS_FILENAME;
        this.csvParser = parser;
    }

    @Override
    public List<Order> getOrderList() {
        return getOrdersFromFile(fileName);
    }

    @Override
    public void saveOrderList(List<Order> orders) {
        writeOrdersToFile(orders, fileName);
    }

    private List<Order> getOrdersFromFile(String fileName) {
        List<Order> orderList = new ArrayList<>();
        try {
            for (String line : Files.readAllLines(Paths.get(fileName), StandardCharsets.UTF_8)) {
                Order order = csvParser.parseLine(line);
                orderList.add(order);
            }
        } catch (IOException e) {
            System.out.println("FICHIER " + fileName + " NON TROUVE. CREATION FICHIER.");
            writeOrdersToFile(orderList, fileName);
        }
        return orderList;
    }

    private void writeOrdersToFile(List<Order> listOrder, String nomFich) {
        List<String> lines = getPrintableOrders(listOrder);

        try {
            Files.write(Paths.get(nomFich), lines, StandardCharsets.UTF_8, StandardOpenOption.CREATE, StandardOpenOption.WRITE);
        } catch (IOException e) {
            System.out.println("PROBLEME AVEC FICHIER");
        }
    }

    private List<String> getPrintableOrders(List<Order> listOrder) {
        List<String> lines = new ArrayList<>();

        for (int i = 0; i < listOrder.size(); i++) {
            Order order = listOrder.get(i);
            String ligneCSV = csvParser.toString(order);
            lines.add(ligneCSV);
        }
        return lines;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
}
