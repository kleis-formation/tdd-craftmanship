package ch.kleis.formation.lags;

import org.junit.Assert;
import org.junit.Test;

import java.text.ParseException;

public class OrderCsvParserTest {

    OrderCsvParser parser = new OrderCsvParser();

    @Test
    public void parseLine_shouldReturnOrder() throws ParseException {
        // given

        // when
        Order order = parser.parseLine("1;2016042;019;32000.0");

        // then
        Assert.assertEquals(new Order("1", "2016042", 19, 32000.0), order);
    }

    @Test(expected = OrderCsvParser.InvalidFormatException.class)
    public void parseLine_invalidSeparator_shouldThrowInvalidFormatException() {
        // given

        // when
        Order order = parser.parseLine("1,2016042,019,32000.0");

        // then
        Assert.fail("exception expected");
    }

    @Test(expected = OrderCsvParser.InvalidDateException.class)
    public void parseLine_invalidDate_shouldThrowInvalidDateException() {
        // given

        // when
        Order order = parser.parseLine("1;Dimanche;019;32000.0");

        // then
        Assert.fail("exception expected");
    }

    @Test(expected = OrderCsvParser.InvalidDaysException.class)
    public void parseLine_invalidDays_shouldThrowInvalidDateException() {
        // given

        // when
        Order order = parser.parseLine("1;2016042;A;32000.0");

        // then
        Assert.fail("exception expected");
    }

    @Test(expected = OrderCsvParser.InvalidPriceException.class)
    public void parseLine_invalidPrice_shouldThrowInvalidDateException() {
        // given

        // when
        Order order = parser.parseLine("1;2016042;032;Hello World");

        // then
        Assert.fail("exception expected");
    }

    @Test
    public void toString_ShouldBeCVSFormated() {
        // given
        Order order = parser.parseLine("1;2016042;032;1000.8");

        // when
        String str = parser.toString(order);

        // then
        Assert.assertEquals("1;2016042;32;1000.80", str);
    }


}
