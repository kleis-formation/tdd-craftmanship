package ch.kleis.formation.lags;

import org.junit.Assert;
import org.junit.Test;

import java.text.ParseException;
import java.util.Arrays;

public class OrderDisplayTest {

    @Test
    public void liste_shouldDisplayOrderList() throws ParseException {
        // given
        OrderDisplay orderDisplay = new OrderDisplay();

        // when
        String orderList = orderDisplay.getOrdersDisplayAsText(Arrays.asList(
                new Order("1", "2016032", 4, 9000.0),
                new Order("3", "2016050", 7, 600.0),
                new Order("2", "2016044", 12, 90.0)
        ));

        // then
        Assert.assertEquals("LISTE DES ORDRES\n"
                            + "      ID    DEBUT DUREE         PRIX\n"
                            + "--------  ------- -----   ----------\n"
                            + "       1  2016032     4    9000.00\n"
                            + "       2  2016044    12      90.00\n"
                            + "       3  2016050     7     600.00\n"
                            + "--------  ------- -----   ----------\n", orderList);
    }
}
