package ch.kleis.formation;

import org.junit.Assert;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.CoreMatchers.*;

/**
 * Le code n'est pas optimisé : les derniers tests retestent les assertions des premiers.
 * Un changement comme "les multiples de 2 doivent renvoyer Ouizz" casse tous les tests en même temps.
 */
public class FizzBuzzTest {

    @Test
    public void getFizzbuzz_2_shouldReturnListWith2Number(){

        // given
        FizzBuzz fizzBuzz = new FizzBuzz();

        // when
        List<String> list = fizzBuzz.getFizzBuzz(2);

        // then
        Assert.assertThat(list, hasItems("1", "2"));
    }

    @Test
    public void getFizzBuzz_3_shouldReturnListWith3ElementsAndFizz() {
        // given
        FizzBuzz fizzBuzz = new FizzBuzz();

        // when
        List<String> list = fizzBuzz.getFizzBuzz(3);

        // then
        Assert.assertThat(list, hasItems("1", "2", "Fizz"));
    }

    @Test
    public void getFizzBuzz_5_shouldReturnListWith5ElementsAndFizzAndBuzz() {
        // given
        FizzBuzz fizzBuzz = new FizzBuzz();

        // when
        List<String> list = fizzBuzz.getFizzBuzz(5);

        // then
        Assert.assertThat(list, hasItems("1", "2", "Fizz", "4", "Buzz"));
    }

    @Test
    public void getFizzBuzz_15_shouldReturnListWith15ElementsAndFizzBuzz() {
        // given
        FizzBuzz fizzBuzz = new FizzBuzz();

        // when
        List<String> list = fizzBuzz.getFizzBuzz(15);

        // then
        Assert.assertThat(list, hasItems("1", "2", "Fizz", "4", "Buzz", "Fizz", "7", "8", "Fizz", "Buzz", "11", "Fizz", "13", "14", "FizzBuzz"));
    }
}
