package ch.kleis.formation.meteo;

import org.junit.Test;

public class MeteoStationWasteTest {

    @Test
    public void veryUnusefullTestWithVeryGoodCoverage() {
        try {
            MeteoStation station = new MeteoStation(new MeteoDaoImpl(), new Average());
            station.getMeteoDataForCity("");
            station.getMeteoDataForCity(null);
        } catch (Exception e) {
            System.out.println("Oh, no ! An error...");
        }
    }
}
