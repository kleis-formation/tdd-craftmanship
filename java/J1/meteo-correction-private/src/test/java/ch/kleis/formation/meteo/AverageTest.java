package ch.kleis.formation.meteo;

import org.junit.*;

import java.util.*;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.*;

public class AverageTest {

    private Average average;

    @Before
    public void setup() {
        average = new Average();
    }

    // Then
    @Test(expected = RuntimeException.class)
    public void testAverageNoValue() {
        // Given + When
        average.average(new ArrayList<Integer>());
    }

    @Test
    public void testAverageNoValueAndExceptionMessageCheck() {
        try {
            // Given + When
            average.average(new ArrayList<Integer>());
            fail("Should not pass !");
        } catch (RuntimeException e) {
            // Then
            assertEquals("Should have one item a least", e.getMessage());
        }
    }

    @Test
    public void testAverageOneValue() {
        // Given
        List<Integer> tData = Arrays.asList(4);

        // When
        int avg = average.average(tData);

        // Then
        assertEquals(4, avg);
    }

    @Test
    public void testAverageSeveralValue() {
        // Given
        List<Integer> tData = Arrays.asList(4, 6);

        // When
        int avg = this.average.average(tData);

        // Then
        assertEquals(5, avg);
    }

    @Test
    public void testAverageWithRoundedUpperValue() {
        // Given
        List<Integer> tData = Arrays.asList(4, 5, 5);

        // When
        int avg = this.average.average(tData);

        // Then
        assertEquals(5, avg);
    }

    @Test
    public void testAverageWithRoundedLowerValue() {
        // Given
        List<Integer> tData = Arrays.asList(4, 4, 5);

        // When
        int avg = this.average.average(tData);

        // Then
        assertEquals(4, avg
        );
    }

    @Test
    public void testAverageWithLotOfValues() {
        List<Integer> tData = new ArrayList<>();
        for (int i = 0; i < 50000; i++) {
            tData.add(3);
            tData.add(5);
        }
        Assert.assertEquals(4, average.average(tData));
    }

    @Test
    public void testAverageWithIntOverflow() {
        List<Integer> tData = new ArrayList<>();
        for (int i = 0; i < 50000; i++) {
            tData.add(30000);
            tData.add(50000);
        }
        Assert.assertEquals(40000, average.average(tData));
    }

    private final int NB = 10000000;
    private final int MAX = Integer.MAX_VALUE - 1;

    @Test
    @Ignore
    public void testWithNotEnoughtPrecisionIsKOWithSmallValueAtEnd() {
        Assert.assertEquals(1073741824, //Erreur ce tests ne passe pas avec cette implem
                            average.average(new MyCollection(NB, MAX, NB, 1)));
    }

    @Test
    public void testWithNotEnoughtPrecisionIsOkWithSmallValueFirst() {
        Assert.assertEquals(1073741824,
                            average.average(new MyCollection(NB, 1, NB, MAX)));
    }

    @Test
    public void testMyCollection() {

        assertEquals(3, new MyCollection(2, 1, 1, 3).size());
        Iterator<Integer> tIt = new MyCollection(2, 1, 1, 3).iterator();
        assertTrue(tIt.hasNext());
        assertEquals(1, (int) tIt.next());
        assertEquals(1, (int) tIt.next());
        assertTrue(tIt.hasNext());
        assertEquals(3, (int) tIt.next());
        assertTrue(!tIt.hasNext());
    }

    /**
     * This collection is a collection of 2 values repeated a number of times.
     * It contains first the value1 for size1 times, then the value2 for size2 times.
     */
    class MyCollection extends AbstractCollection<Integer> {
        private int size1;
        private int value1;
        private int size2;
        private int value2;

        private MyCollection(int pSize1, int pValue1, int pSize2, int pValue2) {
            size1 = pSize1;
            value1 = pValue1;
            size2 = pSize2;
            value2 = pValue2;
        }

        @Override
        public Iterator<Integer> iterator() {
            return new MyIterator();
        }

        @Override
        public int size() {
            return size1 + size2;
        }

        class MyIterator implements Iterator<Integer> {
            private int fullSize = size1 + size2;
            private int curPos = 0;

            public boolean hasNext() {
                return curPos < fullSize;
            }

            public Integer next() {
                curPos++;
                if (curPos <= size1) {
                    return value1;
                } else if (curPos <= fullSize) {
                    return value2;
                } else {
                    throw new NoSuchElementException();
                }
            }

            public void remove() {
                next();
            }

        }

    }
}
