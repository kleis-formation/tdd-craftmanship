package ch.kleis.formation;

public class StringCalc {

    public int add(String operation) {
        if (operation.isEmpty()) {
            return 0;
        }

        if (operation.contains(" ")) { // separation par des espaces
            String[] numbers = operation.split(" ");

            if (!operation.contains("\n")) { // pas de saut de ligne
                int total = 0;
                for (int i = 0; i < numbers.length; i++) {
                    int value = Integer.valueOf(numbers[i]);
                    if (value < 0) {
                        throw new NegativeNumberException();
                    }
                    if (value < 1000) {
                        total += value;
                    }
                }
                return total;
            } else { // separation par des sauts de ligne en plus des espaces
                int total = 0;

                for (int i = 0; i < numbers.length; i++) {
                    if (numbers[i].contains("\n")) {
                        String[] subnumbers = numbers[i].split("\n");
                        for (int j = 0; j < subnumbers.length; j++) {
                            int value = Integer.valueOf(subnumbers[j]);
                            if (value < 0) {
                                throw new NegativeNumberException();
                            }
                            if (value < 1000) {
                                total += value;
                            }
                        }
                    } else {
                        int value = Integer.valueOf(numbers[i]);
                        if (value < 0) {
                            throw new NegativeNumberException();
                        }
                        total += value;
                    }
                }

                return total;
            }
        } else if (operation.contains("\n")) { // separation par des sauts de ligne
            String[] numbers = operation.split("\n");
            int total = 0;
            for (int k = 0; k < numbers.length; k++) {
                int value = Integer.valueOf(numbers[k]);
                if (value < 0) {
                    throw new NegativeNumberException();
                }
                if (value < 1000) {
                    total += value;
                }
            }
            return total;
        } else { // pas de separation
            int value = Integer.valueOf(operation);
            if (value < 0) {
                throw new NegativeNumberException();
            }
            if (value < 1000) {
                return value;
            } else {
                return 0;
            }
        }
        // TODO : gerer d'autres caracteres de separation
    }
}
