package ch.kleis.formation.meteo;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class MeteoDataMatchersTest {


    @Test
    public void isEquivalent_Ok() {
        assertThat(new MeteoData(22, 45, 12),
                MeteoDataMatchers.isEquivalent(new MeteoData(22, 45, 12)));
    }

    @Test
    public void isEquivalent_rejectMinimumDivergence() {
        assertThat(new MeteoData(22, 45, 12),
                is(not(MeteoDataMatchers.isEquivalent(new MeteoData(23, 45, 12)))));
    }
    @Test
    public void isEquivalent_rejectMaximumDivergence() {
        assertThat(new MeteoData(22, 45, 12),
                is(not(MeteoDataMatchers.isEquivalent(new MeteoData(22, 49, 12)))));
    }

    @Test
    public void isEquivalent_rejectMediumsDivergence() {
        assertThat(new MeteoData(22, 45, 12),
                is(not(MeteoDataMatchers.isEquivalent(new MeteoData(22, 45, 15)))));
    }

}