package ch.kleis.formation;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class StringCalcTest {

    private StringCalc stringCalc;

    @Before
    public void setup() {
        stringCalc = new StringCalc();
    }


    @Test
    public void emptyString_ShouldReturnZero() {
        Assert.assertEquals(0, stringCalc.add(""));
    }

    @Test
    public void stringWithSpace_ShouldReturnZero() {
        Assert.assertEquals(0, stringCalc.add("  "));
    }

    @Test
    public void stringWithSpaceAndReturn_ShouldReturnZero() {
        Assert.assertEquals(0, stringCalc.add("  \n  "));
    }


    @Test
    public void singleNumber_ShouldReturnThisNumber() {
        Assert.assertEquals(42, stringCalc.add("42"));
    }

    @Test
    public void singleNumberSurroundedBySpaces_ShouldReturnThisNumber() {
        Assert.assertEquals(42, stringCalc.add(" 42  "));
    }

    @Test
    public void twoNumbersCommaSeparated_ShouldReturnSum() {
        Assert.assertEquals(42, stringCalc.add("21 21"));
    }

    @Test
    public void twoNumbersNewLineAndSpaceSeparated_ShouldReturnSum() {
        Assert.assertEquals(42, stringCalc.add("21 \n 21"));
    }

    @Test
    public void twoNumbersNewLineSeparated_ShouldReturnSum() {
        Assert.assertEquals(42, stringCalc.add("21\n21"));
    }

    @Test
    public void threeNumbersNewLineAndCommaSeparated_ShouldReturnSumOfThree() {
        Assert.assertEquals(42, stringCalc.add("20\n20 2"));
    }

    @Test(expected = NegativeNumberException.class)
    public void negativeNumbersThrowAnException() {
        stringCalc.add("-42 2");
    }

    @Test
    public void numberGreaterThan1000_ShouldBeIgnored() {
        Assert.assertEquals(42, stringCalc.add("1000\n42"));
    }

    @Test
    public void newDelimiter_ShouldBeUsedInsteadOfCommaOrNewLine() {
        // TODO : trop complique, refactorer le code
        Assert.assertEquals(42, stringCalc.add("//#\n21#21"));
    }
}
