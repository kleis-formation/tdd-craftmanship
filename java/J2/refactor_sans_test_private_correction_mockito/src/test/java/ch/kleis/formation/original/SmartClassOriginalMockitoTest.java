package ch.kleis.formation.original;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.anyInt;

@RunWith(PowerMockRunner.class)
@PrepareForTest({SmartClassOriginal.class, System.class})
public class SmartClassOriginalMockitoTest {

    @Test
    public void testHtmlPrinterMissingFirstParam() throws Exception {
        // Given
        PowerMockito.mockStatic(System.class);
        PowerMockito.doThrow(new RuntimeException("Close")).when(System.class);
        System.exit(anyInt());

        try {
            // When
            SmartClassOriginal.main(new String[]{"HtmlPrinter"});
            fail("Should not pass !");
        } catch (RuntimeException e) {
            // Then
            assertEquals("Close", e.getMessage());
        }
        PowerMockito.verifyStatic(System.class);
        System.exit(1);
    }


}
