package ch.kleis.formation.original;

class HtmlPrinter implements Closeable {
    public void print(String s) {
        println("<html> Bonjour " + s + "</html>");
    }

    public void close() {
        println("Close");
    }

    void println(String msg) {
        System.out.println(msg);
    }

}
