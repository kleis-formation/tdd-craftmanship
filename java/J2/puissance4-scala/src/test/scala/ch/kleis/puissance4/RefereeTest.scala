package ch.kleis.puissance4

import org.scalamock.scalatest.MockFactory
import org.scalatest.{FlatSpec, Matchers}

object Pipo {
  def paf = "le chien"
}

class RefereeTest extends FlatSpec with MockFactory with Matchers {
  class MockGrid extends Grid


  behavior of "Referee"

  "getGameStatus" should "grid is full wih not winne => Drawn" in {
    //we need to extends the class to get instance with the default argument (scalamock limit)
    val mockGrid = mock[MockGrid]
    (mockGrid.isFull _).expects().returns(true)

    val referee = Referee(mockGrid, new Analyzer)
    val status = referee.getGameStatus

    status should be(PartyDrawn)
  }

  "getGameStatus" should "grid not full and yellow wins" in {
    //we need to extends the class to get instance with the default argument (scalamock limit)
    val mockGrid = mock[MockGrid]
    val mockAnalyzer = mock[Analyzer]
    (mockAnalyzer.getWinner _).expects(*).returns(Some(Pawn.Yellow))

    val referee = Referee(mockGrid, mockAnalyzer)
    val status = referee.getGameStatus

    status should be(PartyHasWinner(Pawn.Yellow))
  }

  "getGameStatus" should "grid not full and none wins" in {
    //we need to extends the class to get instance with the default argument (scalamock limit)
    val mockGrid = mock[MockGrid]
    val mockAnalyzer = mock[Analyzer]
    (mockGrid.isFull _).expects().returns(false)
    (mockAnalyzer.getWinner _).expects(*).returns(None)

    val referee = Referee(mockGrid, mockAnalyzer)
    val status = referee.getGameStatus

    status should be(PartyRunning)
  }

  "nextPlayer" should "revert the pawn" in {
    val referee = Referee(Grid())

    val nextPlayer = referee.nextPlayer

    nextPlayer should not be(referee.currentPlayer)
  }

  "addPawnInColumn" should "handle three token pushed" in {

    val referee = Referee(Grid())

    val gridString  = referee.addPawnInColumn(1)
      .addPawnInColumn(3)
      .addPawnInColumn(1)
      .grid
      .toString

    gridString should equal(
      """.......
        |.......
        |.......
        |.......
        |.O.....
        |.O.X...
      """.stripMargin.trim)
  }
}
