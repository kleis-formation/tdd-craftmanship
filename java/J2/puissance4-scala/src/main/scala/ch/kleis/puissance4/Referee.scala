package ch.kleis.puissance4

import ch.kleis.puissance4.Pawn.Pawn


/**
defines a sealed trait to get the game status and 3 implemented classes & objects
 */
sealed trait GameStatus

object PartyDrawn extends GameStatus

object PartyRunning extends GameStatus

case class PartyHasWinner(pawn: Pawn) extends GameStatus

/**
  * knows who is currently playing, add a token in the grid, know if the game is over with eventually a winner
  * @param grid
  * @param analyzer
  * @param currentPlayer
  */
case class Referee(grid: Grid, analyzer: Analyzer = new Analyzer, currentPlayer: Pawn = Pawn.Yellow) {

  /**
    * as the referee knows who is to play
    * @param col
    * @return
    */
  def addPawnInColumn(col: Int): Referee = {
    val newGrid = grid.addPawn(col, currentPlayer)
    Referee(newGrid, analyzer, nextPlayer)
  }

  /**
    * the next player to play (Yellow -> Red -> Yellow ....)
    * @return
    */
  def nextPlayer = if (currentPlayer == Pawn.Yellow) Pawn.Red else Pawn.Yellow

  /**
    * has the game a winner? is the party drawn? I it still running?
    *
    * @return
    */
  def getGameStatus: GameStatus = {
    analyzer.getWinner(grid) match {
      case Some(pawn: Pawn) => PartyHasWinner(pawn)
      case None if grid.isFull => PartyDrawn
      case None => PartyRunning
    }

  }
}