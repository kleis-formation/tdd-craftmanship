package ch.kleis.puissance4.exceptions

case class ColumnOverflowException(column:Int) extends Exception(s"$column")
