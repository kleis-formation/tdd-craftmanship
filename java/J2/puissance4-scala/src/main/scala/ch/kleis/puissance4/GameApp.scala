package ch.kleis.puissance4

import scala.annotation.tailrec
import scala.io.StdIn

object GameApp extends App {

  /**
    * loops until the game ends
    * @param referee
    */
  @tailrec
  def play(referee: Referee): Unit = {
    println(referee.grid)

    referee.getGameStatus match {
      case PartyHasWinner(pawn) => println(s"Winner!! $pawn")
      case PartyDrawn => println("party drawn - no winner")
      case PartyRunning =>
        print(s"player ${referee.currentPlayer}, enter token col: ")
        val iCol = StdIn.readLine().toInt
        play(referee.addPawnInColumn(iCol))
    }
  }

  val referee = Referee(Grid())
  play(referee)
}
