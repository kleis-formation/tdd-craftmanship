package ch.kleis.puissance4;

import ch.kleis.puissance4.exceptions.ColumnDoesNotExist;
import ch.kleis.puissance4.exceptions.ColumnFull;

public class Grid {

    public static final int NUMBER_OF_COLUMNS = 7;
    public static final int NUMBER_OF_LINES = 6;
    private Pawn[][] pawns = new Pawn[NUMBER_OF_COLUMNS][NUMBER_OF_LINES];
    private int space = NUMBER_OF_COLUMNS * NUMBER_OF_LINES;

    public Grid() {
        empty();
    }

    public int getColumns() {
        return NUMBER_OF_COLUMNS;
    }

    public int getLines() {
        return NUMBER_OF_LINES;
    }

    public Pawn getPawnForColumnAndLine(int column, int line) {
        return pawns[column - 1][line - 1];
    }

    public void insertPawn(Pawn color, int column) {
        if (column <= 0 || column > NUMBER_OF_COLUMNS)
            throw new ColumnDoesNotExist();

        int line = 0;
        while (line < NUMBER_OF_LINES && pawns[column - 1][line] != Pawn.EMPTY) {
            line++;
        }
        if (line >= NUMBER_OF_LINES) {
            throw new ColumnFull();
        }

        space--;
        pawns[column - 1][line] = color;
    }

    public void empty() {
        for (int i = 0; i < NUMBER_OF_COLUMNS; i++) {
            for (int j = 0; j < NUMBER_OF_LINES; j++) {
                pawns[i][j] = Pawn.EMPTY;
            }
        }
        space = NUMBER_OF_COLUMNS*NUMBER_OF_LINES;
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();

        for (int j = NUMBER_OF_LINES - 1; j >= 0; j--) {
            for (int i = 0; i < NUMBER_OF_COLUMNS; i++) {
                sb.append(pawns[i][j]);
            }
            sb.append("\n");
        }

        return sb.toString();
    }

    public boolean isFull() {
        return space <= 0;
    }
}
