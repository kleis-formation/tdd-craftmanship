package ch.kleis.puissance4;

import ch.kleis.puissance4.exceptions.ColumnDoesNotExist;
import ch.kleis.puissance4.exceptions.ColumnFull;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

/*

possede 7 colonnes et 6 lignes, donc 42 cases
accepte des pions dans une colonne, jusque 6 :
  -> une grille vide ne contient que des cases vides
  -> pion dans une colonne vide,
  -> pion dans une colonne pleine
  -> pion dans une colonne inexistante
peut être vidée (empty)
peut s'afficher (print)

 */
public class GridTest {

    @Test
    public void gridMustHave7Columns() {
        // Given
        Grid grid = new Grid();
        // When + Then
        assertEquals(7, grid.getColumns());
    }

    @Test
    public void gridMustHave6Lines() {
        // Given
        Grid grid = new Grid();
        // When + Then
        assertEquals(6, grid.getLines());
    }

    @Test
    public void emptyGrid_MustContainEmptyBoxes() {
        // Given
        Grid grid = new Grid();

        // When
        grid.empty();

        //Then
        for (int i = 1; i <= grid.getColumns(); i++) {
            for (int j = 1; j <= grid.getLines(); j++) {
                assertEquals(Pawn.EMPTY, grid.getPawnForColumnAndLine(i, j));
            }
        }
    }

    @Test
    public void insertPawnInEmptyColumn_MustInsertPawnInColumnAndAtFirstPlace() {
        // Given
        Grid grid = new Grid();
        Pawn pawn = Pawn.RED;
        int column = 1;

        // When
        grid.insertPawn(pawn, column);

        // Then
        Assert.assertEquals(Pawn.RED, grid.getPawnForColumnAndLine(1, 1));
    }

    @Test
    public void insertPawnInNonEmptyColumn_MustInsertPawnInColumnJustAfterPreviousPawn() {
        // Given
        Grid grid = new Grid();
        int column = 1;

        // When
        grid.insertPawn(Pawn.RED, column);
        grid.insertPawn(Pawn.RED, column);

        // Then
        Assert.assertEquals(Pawn.RED, grid.getPawnForColumnAndLine(1, 2));
    }

    @Test(expected = ColumnFull.class) // Then
    public void insertPawnInFullColumn_MustRejectInsertion() {
        // Given
        Grid grid = new Grid();
        Pawn pawn = Pawn.RED;


        int column = 1;
        for (int i = 1; i <= grid.getLines(); i++) {
            grid.insertPawn(pawn, column);
        }
        // Le coup de trop
        grid.insertPawn(pawn, column);
    }

    @Test(expected = ColumnDoesNotExist.class) // Then
    public void insertPawnInColumnThatDoesNotExist_MustRejectInsertion() {
        // Given
        Grid grid = new Grid();
        Pawn pawn = Pawn.YELLOW;
        // When
        grid.insertPawn(pawn, 42);
    }

    @Test
    public void emptyGrid_MustEmptyAllBoxes() {
        // Given
        Grid grid = new Grid();
        grid.insertPawn(Pawn.YELLOW, 1);
        grid.insertPawn(Pawn.RED, 2);

        // When
        grid.empty();

        // Then
        for (int i = 1; i <= grid.getColumns(); i++) {
            for (int j = 1; j <= grid.getLines(); j++) {
                assertEquals(Pawn.EMPTY, grid.getPawnForColumnAndLine(i, j));
            }
        }
    }

    @Test
    public void getGridDisplay_MustReturnStringRepresentation() {
        // Given
        Grid grid = new Grid();
        grid.insertPawn(Pawn.YELLOW, 1);
        grid.insertPawn(Pawn.RED, 2);
        grid.insertPawn(Pawn.YELLOW, 1);
        grid.insertPawn(Pawn.RED, 1);
        grid.insertPawn(Pawn.YELLOW, 2);
        grid.insertPawn(Pawn.RED, 5);

        // Given
        String gridString = grid.toString();

        // Then
        assertEquals(
                ".......\n" +
                ".......\n" +
                ".......\n" +
                "x......\n" +
                "oo.....\n" +
                "ox..x..\n", gridString);
    }

    @Test
    public void testBadMoveShouldNotFillGrid() {
        // Given
        String gridAsString =
                "xoxxoo.\n" +
                "oxoxoxo\n" +
                "xoxoxox\n" +
                "xoxoxox\n" +
                "oxoxoxo\n" +
                "oxoxoxo\n";
        Grid grid = parseGrid(gridAsString);

        // When
        try {
            grid.insertPawn(Pawn.RED, 6);
            fail("Should not pass !");
        } catch (ColumnFull e) {
        }

        // Then
        assertFalse(grid.isFull());

    }

    @Test
    public void testParseGrid() {
        // Given
        String grid =
                ".......\n" +
                ".......\n" +
                ".......\n" +
                "x......\n" +
                "oo.....\n" +
                "ox..x..\n";

        // When + Then
        assertEquals(grid, parseGrid(grid).toString());
    }

    public static Grid parseGrid(String strGrid) {
        String[] lines = strGrid.split("\n");
        Grid result = new Grid();
        for (int j = result.getLines() - 1; j >= 0; j--) {
            for (int i = 0; i < result.getColumns(); i++) {
                if (lines[j].charAt(i) == 'o') {
                    result.insertPawn(Pawn.YELLOW, i + 1);
                } else if (lines[j].charAt(i) == 'x') {
                    result.insertPawn(Pawn.RED, i + 1);
                }
            }
        }
        return result;
    }


}
