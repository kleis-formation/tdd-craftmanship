/*
 * Copyright (c) 2006 Henri Tremblay
 */
package ch.kleis.formation;

/**
 * Que pensez-vous de ce code? Que feriez-vous pour l'améliorer au niveau 1- Design 2- Fonctionnement prévu (il y a une erreur qui empêche
 * se programme de fonctionner correctement. 3- Best practices et style de programmation
 * 
 * Globalement tout commentaire que tu ferais lors d'une révision de code
 */
public class SmartClass {
	public static class InvalidParameter extends RuntimeException {
		private static final long serialVersionUID = 2110300501088645462L;
		private int exitCode;

		public InvalidParameter(String pMessage, int pExitCode) {
			super(pMessage);
			exitCode = pExitCode;
		}

		public int getExitCode() {
			return exitCode;
		}
	}

	public static class Param {
		public String name;
		public String printerName;

		public Param(String pName, String pPrinterName) {
			name = pName;
			if("HtmlPrinter".equals(pPrinterName) || "TextPrinter".equals(pPrinterName)){
			printerName = pPrinterName;
			} else {
				 throw new InvalidParameter("Invalid second parameter. It should be HtmlPrinter or TextPrinter.", 2);
			}
		}

		public Printer newPrinter() {
			if ("HtmlPrinter".equals(printerName)) {
				return new HtmlPrinter();
			} else if ("TextPrinter".equals(printerName)) {
				return new TextPrinter();
			} else {
				throw new RuntimeException("PrinterName invalide : "+printerName);
			}
		}
}

	public static Param parseParam(String[] args) {
		if (args.length < 2)
			throw new InvalidParameter("Please provide 2 parameters \"(HtmlPrinter|TextPrinter) [1-3]\"", 1);
		try {
			int nameIndex = Integer.parseInt(args[1]);
			return new Param(getName(nameIndex), args[0]);
		} catch (NumberFormatException ex) { // Trap the number format exception
			throw new InvalidParameter("Invalid first parameter. It should be 1, 2 or 3.", 1);
		}
	}

	private static String getName(int nameIndex) {
		switch (nameIndex) {
		case 1:
			return "Henri";
		case 2:
			return "Philippe";
		case 3:
			return "Cedrik";
		default:
			throw new InvalidParameter("Invalid first parameter. It should be 1, 2 or 3.", 1);
		}
	}

	public static void main(String[] args) {
		Printer tPrinter=null;
		try {
			Param tParam = parseParam(args);
			 tPrinter = tParam.newPrinter();
			 tPrinter.print(tParam.name);
		} catch (InvalidParameter e) {
			System.err.println(e.getMessage());
			System.exit(e.exitCode);
		}finally{
			if (tPrinter!=null){
				tPrinter.close();
			}
		}

	}
}

interface Printer {
	public void print(String s);

	void close();
}

abstract class AbstractPrinter implements Printer {
	public void close() {
		System.out.println("Close");
	}
}

class HtmlPrinter extends AbstractPrinter {
	public void print(String s) {
		System.out.println("<html> Bonjour " + s + "</html>");
	}
}

class TextPrinter extends AbstractPrinter {
	public void print(String s) {
		System.out.println("Bonjour " + s);
	}
}
