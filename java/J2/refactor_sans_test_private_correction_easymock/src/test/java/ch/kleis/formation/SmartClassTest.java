package ch.kleis.formation;

import java.io.PrintStream;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.powermock.api.easymock.PowerMock.*;
import static org.junit.Assert.*;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ SmartClass.class, System.class })
public class SmartClassTest {

	// Tests boite noire avant refactoring
	@Test
	public void testHtmlPrinterForPhilippe() throws Exception {
		PrintStream tOut = createMock(PrintStream.class);
		System.setOut(tOut);
		tOut.println("<html> Bonjour Philippe</html>");
		tOut.println("Close");
		replayAll();

		SmartClass.main(new String[] { "HtmlPrinter", "2" });
		verifyAll();
	}

	@Test
	public void testHtmlPrinterMissingFirstParam() throws Exception {
		PrintStream tOut = createMock(PrintStream.class);
		System.setErr(tOut);
		mockStatic(System.class);
		tOut.println("Please provide 2 parameters \"(HtmlPrinter|TextPrinter) [1-3]\"");
		System.exit(1);
		expectLastCall().andThrow(new RuntimeException("Close"));
		replayAll();

		try {
			SmartClass.main(new String[] { "HtmlPrinter" });
		} catch (RuntimeException e) {
			assertEquals("Close", e.getMessage());
		}
		verifyAll();
	}

	@Test
	public void testHtmlPrinterMissingBothParams() throws Exception {
		PrintStream tOut = createMock(PrintStream.class);
		System.setErr(tOut);
		mockStatic(System.class);
		tOut.println("Please provide 2 parameters \"(HtmlPrinter|TextPrinter) [1-3]\"");
		System.exit(1);
		expectLastCall().andThrow(new RuntimeException("Close"));
		replayAll();

		try {
			SmartClass.main(new String[] {});
		} catch (RuntimeException e) {
			assertEquals("Close", e.getMessage());
		}
		verifyAll();
	}

	@Test
	public void testHtmlPrinterInvalidFirstParam() throws Exception {
		PrintStream tOut = createMock(PrintStream.class);
		System.setErr(tOut);
		mockStatic(System.class);
		tOut.println("Invalid second parameter. It should be HtmlPrinter or TextPrinter.");
		System.exit(2);
		expectLastCall().andThrow(new RuntimeException("Close"));
		replayAll();

		try {
			SmartClass.main(new String[] { "BadPrinter", "2" });
		} catch (RuntimeException e) {
			assertEquals("Close", e.getMessage());
		}
		verifyAll();
	}

	@Test
	public void testHtmlPrinterInvalidSecondParam() throws Exception {
		PrintStream tOut = createMock(PrintStream.class);
		System.setErr(tOut);
		mockStatic(System.class);
		tOut.println("Invalid first parameter. It should be 1, 2 or 3.");
		System.exit(1);
		expectLastCall().andThrow(new MyExitException("Close"));
		replayAll();

		try {
			SmartClass.main(new String[] { "HtmlPrinter", "r" });
		} catch (MyExitException e) {
			assertEquals("Close", e.getMessage());
		}
		verifyAll();
	}

	public class MyExitException extends RuntimeException {
		private static final long serialVersionUID = 3496502698353044974L;

		public MyExitException(String pMessage) {
			super(pMessage);
		}
	}

}
