package ch.kleis.formation.tips;

import org.easymock.EasyMockSupport;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import java.lang.reflect.Field;

import static org.easymock.EasyMock.expect;
import static org.junit.Assert.assertEquals;

public class StockTest extends EasyMockSupport {

    @Test
    public void getValueTestWithMockGetQuantity() {
        // Given
        Stock stock = partialMockBuilder(Stock.class)
                .addMockedMethod("getPrice") // Mock REST call
                .addMockedMethod("getQuantity") // Mock accessor
                .createMock();
        expect(stock.getPrice()).andReturn(5.0);
        expect(stock.getQuantity()).andReturn(2);
        replayAll();

        // When
        double value = stock.getValue();

        // Then
        assertEquals(10.0, value, 1E-3);
        verifyAll();
    }


    @Test
    public void getValueTestWithMockConstructor() {
        // Given
        Stock stock = partialMockBuilder(Stock.class)
                .withConstructor(2)
                .addMockedMethod("getPrice") // Mock REST call
                .createMock();
        expect(stock.getPrice()).andReturn(5.0);
        replayAll();

        // When
        double value = stock.getValue();

        // Then
        assertEquals(10.0, value, 1E-3);
        verifyAll();
    }

    @Test
    public void getValueTestWithReflection() throws NoSuchFieldException, IllegalAccessException {
        // Given
        Stock stock = new Stock(5) {
            @Override
            public double getPrice() {
                return 3;
            }
        };
        Field qty = Stock.class.getDeclaredField("quantity");
        qty.setAccessible(true);
        qty.set(stock, 2);

        // When
        double value = stock.getValue();

        // Then
        assertEquals(6.0, value, 1E-3);
    }

    @Test
    public void getValueTestWithReflectionTestUtil() throws NoSuchFieldException, IllegalAccessException {
        // Given
        Stock stock = new Stock(5) {
            @Override
            public double getPrice() {
                return 3;
            }
        };

        ReflectionTestUtils.setField(stock, "quantity", 2);

        // When
        double value = stock.getValue();

        // Then
        assertEquals(6.0, value, 1E-3);
    }

}