package ch.kleis.formation.original;

import java.io.PrintStream;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.powermock.api.easymock.PowerMock.*;
import static org.junit.Assert.*;

@RunWith(PowerMockRunner.class)
@PrepareForTest({SmartClassOriginal.class})
public class SmartClassOriginalEasyMockTest {

    @Test
    public void testHtmlPrinterForHenri() throws Exception {
        HtmlForPeople("1", "Henri");
    }

    @Test
    public void testHtmlPrinterForPhilippe() throws Exception {
        HtmlForPeople("2", "Philippe");
    }

    @Test
    public void testHtmlPrinterForCedrik() throws Exception {
        HtmlForPeople("3", "Cedrik");
    }

    // JUnit 5 : http://junit.org/junit5/docs/current/user-guide/#writing-tests-parameterized-tests
    private void HtmlForPeople(String id, String name) {
        // Given
        PrintStream tOut = createMock(PrintStream.class);
        System.setOut(tOut);
        tOut.println("<html> Bonjour "+name+"</html>");
        tOut.println("Close");
        replayAll();

        // When
        SmartClassOriginal.main(new String[]{"HtmlPrinter", id});

        // Then
        verifyAll();
    }

    @Test
    public void testTextPrinterForCedrikWithSetOut() {
        // Given
        PrintStream tOut = createMock(PrintStream.class);
        System.setOut(tOut);
        tOut.println("Bonjour Cedrik");
        tOut.println("Close");
        replayAll();

        // When
        SmartClassOriginal.main(new String[]{"TextPrinter", "3"});

        // Then
        verifyAll();
    }

    @Test
    public void testTextPrinterForCedrikWithNewPowerMock() throws Exception {
        // Given
        TextPrinter mock = createNiceMock(TextPrinter.class);
        expectNew(TextPrinter.class).andReturn(mock);
        mock.print("Cedrik");
//        mock.close(); Not necessary, as it's a nice mock
        replayAll();

        // When
        SmartClassOriginal.main(new String[]{"TextPrinter", "3"});

        // Then
        verifyAll();
    }

    @Test
    public void testHtmlPrinterMissingFirstParam() throws Exception {
        // Given
        mockStatic(System.class);
        System.exit(1);
        expectLastCall().andThrow(new RuntimeException("Close"));
        replayAll();

        try {
            // When
            SmartClassOriginal.main(new String[]{"HtmlPrinter"});
            fail("Should not pass !");
        } catch (RuntimeException e) {
            // Then
            assertEquals("Close", e.getMessage());
        }
        verifyAll();
    }

    @Test
    public void testHtmlPrinterMissingBothParams() throws Exception {
        //Given
        mockStatic(System.class);
        System.exit(1);
        expectLastCall().andThrow(new MyExitException("Close"));
        replayAll();

        try {
            // When
            SmartClassOriginal.main(new String[]{});
            fail("Should not pass !");
        } catch (MyExitException e) {
            // Then
            assertEquals("Close", e.getMessage());
        }
        verifyAll();
    }

    @Test
    public void testHtmlPrinterInvalidFirstParam() throws Exception {
        try {
            SmartClassOriginal.main(new String[]{"BadPrinter", "2"});
            fail("Should not pass !");
        } catch (NullPointerException e) {
            assertNull(e.getMessage());
        }
    }

    @Test
    public void testHtmlPrinterInvalidSecondParam() throws Exception {
        mockStatic(System.class);
        System.exit(1);
        expectLastCall().andThrow(new MyExitException("Close"));
        replayAll();

        try {
            SmartClassOriginal.main(new String[]{"HtmlPrinter", "r"});
            fail("Should not pass !");
        } catch (MyExitException e) {
            assertEquals("Close", e.getMessage());
        }
    }

    @Test
    @Ignore // Ce test ne passe pas à cause du bug du '==' au lieu d'un 'equals'
    public void testShowTheBugWithEquals() throws Exception {
        // Given
        PrintStream tOut = createMock(PrintStream.class);
        System.setOut(tOut);
        tOut.println("Bonjour Cedrik");
        tOut.println("Close");
        replayAll();

        // When
        SmartClassOriginal.main(new String[]{new String("TextPrinter"), "3"});

        // Then
        verifyAll();
    }

    public class MyExitException extends RuntimeException {
        private static final long serialVersionUID = 3496502698353044974L;

        public MyExitException(String pMessage) {
            super(pMessage);
        }
    }

}
